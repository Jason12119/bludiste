<?php

/**
 * Description of maze - Trida, generujici bludiste, ve kterem najde cil.
 * 
 * @author Petr Skvarcek I4 2015
 * 
 * 
 */
class maze {

    var $pole = array();     
    var $xmax;
    var $ymax;
    var $prekazky;
    var $startx;
    var $starty;
    var $kroky;
    /**
     * 
     * @param int $xmax Maximalni sirka
     * @param int $ymax Maximalni vyska
     * @param int $prekazky Pocet prekazek
     */
    public function __construct($xmax, $ymax, $prekazky) {
        $this->xmax = $xmax;
        $this->ymax = $ymax;
        $this->prekazky = $prekazky;
    }
/**
 * Description:
 * Generuje v poli retezec CIL.
 */
    public function generujCil() {
        for ($x = 1; $x < $this->xmax + 1; $x++) {

            for ($y = 1; $y < $this->ymax + 1; $y++) {
                $this->pole[$x][$y] = 0;
            }
            echo "</tr>";
        }
        $this->pole[$x = rand(1, $xmax)][$y = rand(1, $ymax)] = "CIL";
    }
/**
 * Description:
 * Generuje v poli retezec START.
 */
    public function generujStart() {
        do {
            $rx = mt_rand(1, $this->xmax);
            $ry = mt_rand(1, $this->ymax);
        } while ($this->pole[$rx][$ry] === "CIL");
        $this->pole[$rx][$ry] = "START";
        $this->startx = $rx;
        $this->starty = $ry;
    }
    /**
 * Description:
 * Generuje v poli retezec #.
 */
    public function generujPrekazky()
    {
         for($i=1; $i <= $this->prekazky; $i++)
            {
                do{
                    $rx = mt_rand(1, $this->xmax);
                    $ry = mt_rand(1, $this->ymax);
                }while((($this->pole[$rx][$ry] === "CIL") || ($this->pole[$rx][$ry] === "START") || $this->pole[$rx][$ry] === "#"));
                $this->pole[$rx][$ry] = "#";
            }
    }
/**
 * Description:
 * Prochazi pole a generuje cisla v poli podle poctu kroku ve vydalenosti vuci startu.
 */
    public function projdiPole() {
        if (( $this->pole[$this->startx + 1][ $this->starty] === 0) && ( $this->pole[$this->startx + 1][ $this->starty] !== "CIL"))
             $this->pole[$this->startx + 1][ $this->starty] = 1;

        if (( $this->pole[$this->startx - 1][ $this->starty] === 0) && ( $this->pole[$this->startx - 1][ $this->starty] !== "CIL"))
             $this->pole[$this->startx - 1][ $this->starty] = 1;

        if (( $this->pole[$$this->startx][ $this->starty + 1] === 0) && ( $this->pole[$this->startx][ $this->starty + 1] !== "CIL"))
             $this->pole[$this->startx][ $this->starty + 1] = 1;

        if (( $this->pole[$this->startx][ $this->starty - 1] === 0) && ( $this->pole[$this->startx][ $this->starty + 1] !== "CIL"))
             $this->pole[$this->startx][ $this->starty - 1] = 1;
        
        for($i=1; $i <= $this->xmax; $i++)
        {
            for($y=1;$y <= $this->ymax; $y++)
            {
                for($x=1;$x <= $this->xmax; $x++)
                {   
                    if( $this->pole[$x][$y]==$i)
                    {
                if(( $this->pole[$x+1][$y]===0) && ( $this->pole[$x+1][$y]!=="CIL"))
                 $this->pole[$x+1][$y]=$i+1;
                
                if(( $this->pole[$x-1][$y]===0) && ( $this->pole[$x-1][$y]!=="CIL"))
                     $this->pole[$x-1][$y]=$i+1;
                
                if(( $this->pole[$x][$y+1]===0) && ( $this->pole[$x][$y+1]!=="CIL"))
                     $this->pole[$x][$y+1]=$i+1;
                
                if(( $this->pole[$x][$y-1]===0) && ( $this->pole[$x][$y-1]!=="CIL"))
                     $this->pole[$x][$y-1]=$i+1;
                
                if(($this->pole[$x+1][$y]==="CIL")||($this->pole[$x-1][$y]==="CIL") || ($this->pole[$x][$y+1]==="CIL") || ($this->pole[$x][$y-1]==="CIL"))
                    $this->kroky = $i;
                    }
                }
            }
        }
    }
    /**
     * Destription:
     * Vypise pole formou tabulky.
     */
    public function vypisPole(){
        echo"<br>";
             echo "<table style='border:1px solid;'>";
            for ($x = 1; $x < $this->xmax + 1; $x++) {
                echo "<tr style='border:1px solid;'>";
                for ($y = 1; $y < $this->ymax + 1; $y++) {
                    echo "<td style='border:1px solid;'>" . $this->pole[$x][$y] . "</td>";
                }
                echo "</tr>";
            }
            echo "</table>";
            echo "<br>"; 
    }

  public function __toString() {
      
      return "";
  }
    /**
     * Description:
     * Vypise pocet kroku vuci startu ve forme retezce.
     */
    public function kroky(){
        echo "Cesta k cili je dlouha ".$this->kroky." kroku.";
    }
}
